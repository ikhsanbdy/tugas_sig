<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->group(['namespace' => 'App\Http\Controllers\Api\Geoserver', 'prefix' => 'api/geoserver'], function ($app) {
	$app->get('admingk', 'GeoserverController@getAdminGk');
	$app->get('admintest', 'GeoserverController@getAdminTest');
	$app->get('dbd_seluruh', 'GeoserverController@getDbKeseluruhan');
	$app->get('dbd_bulan', 'GeoserverController@getDbBulan');
});

$app->group(['namespace' => 'App\Http\Controllers\Api', 'prefix' => 'api'], function ($app) {
	
});

$app->get('test/leaflet', function () use ($app) {
    return view('test_leaflet');
});

$app->get('test/google', function () use ($app) {
    return view('test_google');
});

$app->get('/', function () use ($app) {
    return view('main_dashboard');
});