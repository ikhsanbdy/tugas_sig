<?php

namespace App\Http\Controllers\Api\Geoserver;

use App\Http\Controllers\Controller;

use App\Libraries\Curl;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Validator;

class GeoserverController extends Controller
{
    /**
     * .
     *
     * @param
     * @return Response
     */
    public function getAdminGk()
    {
        try 
        {
            $data = Curl::fetch('http://localhost:8080/geoserver/tugas_sig/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=tugas_sig:admin_gunungkidul&outputFormat=application%2Fjson');
        }
        catch(\Exception $e)
        {
            $data = null;
        }

        return $data;
    }

    /**
     * .
     *
     * @param
     * @return Response
     */
    public function getDbKeseluruhan()
    {
        try 
        {
            $data = Curl::fetch('http://localhost:8080/geoserver/tugas_sig/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=tugas_sig:dbd_keseluruhan&outputFormat=application%2Fjson');
        }
        catch(\Exception $e)
        {
            $data = null;
        }

        return $data;
    }

    /**
     * .
     *
     * @param
     * @return Response
     */
    public function getDbBulan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year' => 'required|integer',
            'month' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return null;
        }

        try
        {
            $data = Curl::fetch('http://localhost:8080/geoserver/tugas_sig/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=tugas_sig:db_bulan&viewparams=month:' . $request->input('month') . ';year:' . $request->input('year') . '&outputFormat=application%2Fjson');
        }
        catch(\Exception $e)
        {
            $data = null;
        }

        return $data;
    }

    /**
     * .
     *
     * @param
     * @return Response
     */
    public function getAdminTest()
    {
        try 
        {
            $data = Curl::fetch('http://localhost:8080/geoserver/Test/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=Test:baleharjo&outputFormat=application%2Fjson');
        }
        catch(\Exception $e)
        {
            $data = null;
        }
        
        return $data;
    }
}