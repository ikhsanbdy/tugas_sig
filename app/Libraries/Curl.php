<?php

namespace App\Libraries;

class Curl
{

	public static function fetch($url)
	{
		try
		{
			$curl = curl_init();

	        $opt = [
	            CURLOPT_URL => $url,
	            CURLOPT_ENCODING => 'gzip,deflate,sdch',
	            CURLOPT_AUTOREFERER => true,
	            CURLOPT_FOLLOWLOCATION => 1,
	            CURLOPT_SSL_VERIFYPEER => false,
	            CURLOPT_BINARYTRANSFER => true,
	            CURLOPT_RETURNTRANSFER => 1
	        ];

	        curl_setopt_array($curl, $opt);
	        
	        $response = curl_exec($curl);
	        $err = curl_error($curl);

	        curl_close($curl);

	        if ($err)
	        {
	            throw new \Exception($err);
	        }

	        return $response;
        }
        catch (\Exception $e) 
        {
			throw new \Exception($e->getMessage());
        }
	}
}