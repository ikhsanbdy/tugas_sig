<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenderitaDbdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penderita_dbd', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->float('umur')->nullabel();
            $table->string('jenis_kelamin')->nullable();
            $table->string('dusun')->nullable();
            $table->integer('kelurahan_gid');
            $table->string('puskesmas')->nullable();
            $table->date('tanggal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('penderita_dbd');
    }
}
