<!DOCTYPE HTML>
<html ng-app="tugas-sig">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIG Persebaran Penderita Demam Berdarah Kabupaten Gunung Kidul</title>

    <link rel="stylesheet" href="assets/css/pure/pure-min.css">
    <link rel="stylesheet" href="assets/css/pure/grids-responsive-min.css">
    <link rel="stylesheet" href="assets/css/leaflet/leaflet.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
	<div class="pure-g full-height" ng-controller="dashboard">
		<div class="sidebar pure-u-1 pure-u-md-1-4">
			<div class="l-box">
				<div class="app-title">Hello World</div>
				<div class="pure-menu pure-menu-horizontal">
				    <ul class="pure-menu-list">
				        <li class="pure-menu-item"><a href="#" class="pure-menu-link">Menu</a></li>
				        <li class="pure-menu-item"><a href="#" class="pure-menu-link">Menu</a></li>
				        <li class="pure-menu-item"><a href="#" class="pure-menu-link">Menu</a></li>
				    </ul>
				</div>
			</div>
	    </div>

	    <div class="content pure-u-1 pure-u-md-3-4">
	    	<!--<div ng-view></div>-->
	    	<div>
				<div id="map"></div>
			</div>

            <div class="footer legal">
            	<div id="indicator">
					<ul>
						<li ng-repeat="(level, style) in layerLevel">
							<div ng-if="level == 'medium'" ng-style="{'background-color': style.fillColor, 'opacity': (style.fillOpacity + 0.1)}"></div>
							<div ng-if="level != 'medium'" ng-style="{'background-color': style.color, 'opacity': (style.fillOpacity + 0.1)}"></div>
							<span>{{level}}<span>
						</li>
					</ul>
				</div>
		        <p class="legal-copyright">
		            &copy; 2015 Kelompok SIG.
		        </p>
			</div>
	    </div>
	</div>

	<script src="assets/js/jquery/jquery-2.1.4.min.js"></script>

	<script src="assets/js/angular/angular.min.js"></script>

	<script src="assets/js/leaflet/leaflet.js"></script>
	<script src="assets/js/leaflet/leaflet.ajax.min.js"></script>

	<script src="assets/js/layer-style.js"></script>

	<script src="assets/js/app.js"></script>
	
</body>
</html>