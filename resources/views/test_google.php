<!DOCTYPE html>
<html>
  <head>
    <title>Data Layer: Simple</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>

	var map;
	function initMap() {
	  map = new google.maps.Map(document.getElementById('map'), {
		zoom: 10,
		center: {lat: -7.976162, lng: 110.622434} 
	  });

	  // NOTE: This uses cross-domain XHR, and may not work on older browsers.
	  map.data.loadGeoJson('http://localhost:8000/api/geoserver/admingk').data;
	}

		</script>
		<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1bJwRH3rGDCLGqQ07HT5YMmiyI9GfnMo&signed_in=true&callback=initMap"></script>
  </body>
</html>