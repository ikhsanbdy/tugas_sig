<html>
	<title>Peta Jumlah Penduduk Indonesia</title>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="../assets/css/leaflet/leaflet.css" />
		<style>
		 html { height: 100% }
		 body { height: 100%; margin: 0; padding: 0 }
			 #map{ height: 100% } 
		</style>
	</head>

	<body>
		<body onload="initialize()">

			<div id="map"></div>  

		</body>
		
		<script src="../assets/js/jquery/jquery-2.1.4.min.js"></script>
		<script src="../assets/js/leaflet/leaflet.js"></script>
		<script>
			//variabel untuk menampung peta
			 var map;
			 //variabel untuk menampung polygon provinsi
			 var layerprovinsi;
			 //array untuk menampung idprovinsi, nama provinsi, dan jumlah penduduk
			 var idProvinsi=new Array();
			 var namaProvinsi=new Array();
			 var jp=new Array();
			 //style untuk polygon
			 var styleProvinsi ={
			   'color':'#00ff00',
			   'opacity':0.1,
			   'weight':4,
			  }
			 var styleSelected ={
			   'color':'#0000ff',
			   'opacity':0.1,
			   'weight':4,
			  }
			 var styleHigh ={
			   'color':'#ff0000',
			   'opacity':0.1,
			   'weight':4,
			  }
			 var styleMed ={
			   'color':'#ffff00',
			   'opacity':0.1,
			   'weight':4,
			  }
			 var styleLow ={
			   'color':'#00ff00',
			   'opacity':0.1,
			   'weight':4,
			  }

			  function initialize()
			  {
			   //panggil base map
			   map = L.map('map').setView([-7.976162, 110.622434], 11);
			   L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
				   attribution: '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
			   }).addTo(map);
			   //panggil function callJumlahPenduduk
			   //callJumlahPenduduk(); 
			   callProvMap();

			  }
			  function callJumlahPenduduk()
			  {
			   //ajax untuk mendapatkan data jumlah penduduk
			   $.ajax({
				url: 'http://localhost/tematikIndonesia/datasql.php?id=1',
				type : 'POST',
				dataType : 'json',
				success: function(data)    
				{      
				 //simpan data jumlah penduduk ke dalam array
				 for(var i=0;i<data.data.length;i++)
				 {
				  idProvinsi[i]=data.data[i].idprovinsi;
				  namaProvinsi[i]=data.data[i].namaprovinsi;
				  jp[i]=data.data[i].jp;
				 }
				 //panggil function callProvMap() 
				 callProvMap();    
				}
			   }); 
			  }
			  
			  function callProvMap()
			  { 
			   //ajax untuk memanggil polygon provinsi dari geoserver
			   $.ajax({
				url: 'http://localhost:8000/api/geoserver/admingk',
				type : 'GET',
				dataType : 'json',
				success: function(data)    
				{
				console.log(data);
				 //tampilkan polygon provinsi, setiap polygon mempunyai fitur yang ada pada function onEachFeature
				 layerprovinsi=L.geoJson(data,{style:styleProvinsi,onEachFeature:onEachFeature}).addTo(map);
				 
				 //event ketika polygon di klik
				 layerprovinsi.on('click', function(e) {
				  //layerprovinsi.setStyle(styleProvinsi);
					 map.setView(e.latlng);        
					 //e.layer.setStyle(styleSelected);      
					});        
				}
			   });      
				
			  }

			  function onEachFeature(feature, layer)
			  {
			   //tampung kodeprovinsi tiap polygon
			   /*var kodeprovinsi=layer.feature.properties.KODE;
			   var jumlah=0;
			   //mengatur warna polygon berdasarkan jumlah penduduk nya
			   for(var i=0;i<idProvinsi.length;i++)
			   {
				if(kodeprovinsi==idProvinsi[i])
				{
				 if(jp[i]<3000000)
				 {
				  layer.setStyle(styleLow);       
				 }
				 else if(jp[i]>=3000000&&jp[i]<6000000)
				 {
				  layer.setStyle(styleMed);
				 }
				 else if(jp[i]>=6000000)
				 {
				  layer.setStyle(styleHigh);
				 }
				 jumlah=jp[i];     
				}
			   }*/
			   console.log(feature);
			   layer.setStyle(styleLow);
			   //menambahkan info window ketika polygon di klik
			   //informasi yang ditampilkan adalah nama provinsi dan jumlah penduduk
			   //layer.bindPopup("<b>"+layer.feature.properties.PROPINSI_+"</b>"+jumlah+" Jiwa");
			  }
			  
		</script>
	</body>
</html>