'use strict';

// angularjs
var app = angular.module('tugas-sig', []);

var api_url = "http://" + window.location.host + "/api/";

app.controller('dashboard', function ($scope, $http, $location)
{
	var map, mLayer;
    $scope.layerLevel = layerStyle.level;

	$scope.init = function() {
		map = L.map('map').setView([-7.976162, 110.622434], 11);
	    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
			attribution: '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
	    }).addTo(map);

    	$scope.loadGeoJsonAll();

    	// setTimeout(function() {
    	// 	$scope.refreshGeoJsonMonth(2015, 1);
    	// }, 3000);
    };

    $scope.loadGeoJsonAll = function() {
		mLayer = L.geoJson.ajax(api_url + 'geoserver/dbd_seluruh',  {
			style: layerStyle.default, 
			onEachFeature: onEachFeature
		}).addTo(map);

		mLayer.on('mouseover', function(e) {
			var nHoverColor = layerStyle.hovered;
			e.layer.oldOptions = e.layer.options;
			nHoverColor.fillColor = (e.layer.options.fillColor == null) ? e.layer.options.color : e.layer.options.fillColor;
			nHoverColor.fillOpacity = e.layer.options.fillOpacity;
			e.layer.setStyle(nHoverColor);
		});

		mLayer.on('mouseout', function(e) {
			if(e.layer.hasOwnProperty('oldOptions')) {
				e.layer.setStyle(e.layer.oldOptions);      
			}
		});
    };

    $scope.refreshGeoJsonMonth = function($year, $month) {
    	mLayer.refresh(api_url + 'geoserver/db_bulan?month=' + $month + '&year=' + $year);
    };

    $scope.init();

    function onEachFeature(feature, layer) {
    	var num = feature.properties.jumlah_penderita || 0;

    	if(num <= 5) {
    		layer.setStyle(layerStyle.level.low);
    	}
    	else if(num > 5 && num <= 10) {
    		layer.setStyle(layerStyle.level.medium);
    	}
    	else {
    		layer.setStyle(layerStyle.level.high);
    	}

    	layer.feature = feature.properties;

    	var popup = '<b>Kelurahan ' + feature.properties.nama_kelurahan + '</b>'
    		+ '<div class="penderita-label">Penderita DBD: <b>' + num + '</b> jiwa</div>';
    	layer.bindPopup(popup);
    }
});