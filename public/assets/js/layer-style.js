// var defaultColor = {
// 	'color': '#00ff00',
// 	'opacity': 0.6,
// 	'fillOpacity': 0.5,
// 	'weight': 2,
// };

// var selectedColor = {
// 	'color': '#2196F3',
// 	'opacity': 0.5,
// 	'fillOpacity': 0.4,
// 	'weight': 2,
// };

// var hoverColor = {
// 	'color': '#000000',
// 	'opacity': 0.6,
// 	'weight': 2,
// };

// var highColor = {
// 	'color': '#F44336',
// 	'opacity': 0.7,
// 	'fillOpacity': 0.6,
// 	'weight': 2,
// };

// var mediumColor = {
// 	'color': '#F9A825',
// 	'fillColor': '#FFEB3B',
// 	'opacity': 0.7,
// 	'fillOpacity': 0.4,
// 	'weight': 2,
// };

// var lowColor = {
// 	'color': '#4CAF50',
// 	'opacity': 0.6,
// 	'fillOpacity': 0.4,
// 	'weight': 2,
// };

var layerStyle = {};

layerStyle['default'] = {
	'color': '#00ff00',
	'opacity': 0.6,
	'fillOpacity': 0.5,
	'weight': 2,	
};

layerStyle['selected'] = {
	'color': '#2196F3',
	'opacity': 0.5,
	'fillOpacity': 0.4,
	'weight': 2,
};

layerStyle['hovered'] = {
	'color': '#000000',
	'opacity': 0.6,
	'weight': 2,
};

layerStyle['level'] = {};

layerStyle['level']['high'] = {
	'color': '#F44336',
	'opacity': 0.7,
	'fillOpacity': 0.6,
	'weight': 2,
};

layerStyle['level']['medium'] = {
	'color': '#F9A825',
	'fillColor': '#FFEB3B',
	'opacity': 0.7,
	'fillOpacity': 0.4,
	'weight': 2,
};

layerStyle['level']['low'] = {
	'color': '#4CAF50',
	'opacity': 0.6,
	'fillOpacity': 0.4,
	'weight': 2,
}