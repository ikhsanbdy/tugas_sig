## Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

## Instalasi

Requirement:

* [Git](https://git-scm.com/download/win)

* [Composer](https://getcomposer.org/Composer-Setup.exe)

* Postgres with Postgis extension

* [Geoserver](http://geoserver.org/release/stable)


Langkah-langkah:

* Pastikan semua requirement di atas telah terinstall.

* Buka Git Bash, masuk ke direktori dimana source akan diinstall, ex:
```
#!git

cd /c/projects
```

* Clone repository ini, dengan copy paste:
```
#!git

git clone https://ikhsanbudiyanto@bitbucket.org/ikhsanbudiyanto/tugas_sig.git
```

* Selanjutnya masuk ke direkteori tugas_sig, lalu ketik di Git Bash:
```
#!git

composer install
```

* Buka pgAdmin, import file sql di source proyek direktori /storage/data/dbd_gunung_kidul.sql.

* Ekstrak file tugas_sig.zip di folder yang sama, lalu copy kan ke folder workspaces pada direktori instalasi geoserver, (contoh "C:\Program Files\GeoServer 2.8.1\data_dir\workspaces").

* Jalankan geoserver, masuk melalui browser ke localhost:8080/geoserver. Klik menu Stores, lalu klik dbd_gunung_kidul, ubah user dan password postgres sesuai dengan user postgres masing-masing.

* Pada Git Bash, ketik: 
```
#!git

php artisan serve
```

* Buka browser, ketik localhost:8000.